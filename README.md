# 3d-scanner

A tool for creating 3D models of real life objects with any camera.
The results aren't too impressive, but I guess it sort of works.

## Dependencies
* python3
* pygame
* numpy
* scipy
* blender

## How to use
I'm running this program on Debian GNU/Linux,
but I'm pretty sure it can run without any modifications on all *NIX systems.
If you want to run it on Windows I guess you have to run what's in `generate_3d_model.sh` manually,
assuming you have blender in $PATH or use the path to the executable.
I don't own a Windows machine to make sure it works though. WSL probably works too.

1. Mark points on the object you want to scan like in the provided example images.
2. Take two pictures of the object moving the camera slightly to the side between shots.
Try not to rotate the camera.
3. Put the pictures in the same directory as the program
and name them `0.jpg` and `1.jpg` replacing the example ones.
4. Run `pick_points.py` and follow the displayed instructions.
After pressing "DONE" the points are saved to `points.json`.
5. Run `generate_3d_model.sh`. This program reads the json file
and creates a blender model of the scanned object.
Additionally, you can create `points.json` yourself and skip step 3 and 4.

Images are licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

