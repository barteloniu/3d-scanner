#!/usr/bin/python3

# 3d-scanner
# Copyright (C) 2019  Bartek Błaszczyk
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pygame
import sys
import math
import json


# Made with vertical images in mind.
MAX_IMG_HEIGHT = 550
IMG_GAP = 10
TEXT_SIZE = 30
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GREEN = (0, 150, 0)
MSGS = [
    "Click on a point on the first image.",
    "Click on another point on the first image.",
    "Click on the same point but on the second image."
]

pygame.init()

imgs = []
for i in range(2):
    imgs.append(pygame.image.load(f"{i}.jpg"))
    size = (
        math.floor(MAX_IMG_HEIGHT / imgs[i].get_height() * imgs[i].get_width()),
        MAX_IMG_HEIGHT
    )
    imgs[i] = pygame.transform.scale(imgs[i], size)

font = pygame.font.Font(None, TEXT_SIZE)

finish_button = font.render(" DONE ", True, WHITE, GREEN)
finish_button_rect = None

size = (
    imgs[0].get_width() + imgs[1].get_width() + IMG_GAP,
    MAX_IMG_HEIGHT + finish_button.get_height()
)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("3d-scanner")

can_stop_picking = False

def draw(msg):
    global finish_button_rect
    screen.fill(BLACK)
    for i in range(2):
        screen.blit(imgs[i], ((imgs[0].get_width() + IMG_GAP) * i, 0))
    text = font.render(msg, True, WHITE)
    screen.blit(text, (4, MAX_IMG_HEIGHT))
    pos = (
    	screen.get_width() - finish_button.get_width(),
    	MAX_IMG_HEIGHT
    )
    if can_stop_picking:
        finish_button_rect = screen.blit(finish_button, pos)
    pygame.display.flip()

draw(MSGS[0])

points = [[], []]
clicks = 0

picking_points = True
while picking_points:
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONUP:
            if (can_stop_picking
                    and event.pos[0] > finish_button_rect[0]
                    and event.pos[1] > finish_button_rect[1]):
                picking_points = False
                continue

            if clicks % 2 == 0:
                points[0].append((event.pos[0], event.pos[1]))
                can_stop_picking = False
                draw(MSGS[2])
            else:
                pos = (
                    event.pos[0] - imgs[0].get_width() - IMG_GAP,
                    event.pos[1]
                )
                points[1].append(pos)
                if len(points[1]) > 2:
                    can_stop_picking = True
                draw(MSGS[1])
            clicks += 1
        elif event.type == pygame.QUIT:
            sys.exit()

with open("points.json", "w") as f:
    json.dump(points, f)

