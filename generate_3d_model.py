# 3d-scanner
# Copyright (C) 2019  Bartek Błaszczyk
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import math
import numpy as np
import scipy.spatial
import bpy

points = None
with open("points.json", "r") as f:
    points = json.load(f)

diff = []
for i in range(len(points[0])):
    x = points[0][i][0] - points[1][i][0]
    y = points[0][i][1] - points[1][i][1]
    diff.append(math.sqrt(x ** 2 + y ** 2))
smallest = min(diff)
for i in range(len(diff)):
    diff[i] -= smallest

vertices = []
for index, p in enumerate(points[0]):
    vertices.append([p[0] / 100, p[1] / 100, diff[index] / 10])
faces = scipy.spatial.Delaunay(np.array(points[0])).vertices.tolist()

mesh = bpy.data.meshes.new("Scan")
obj = bpy.data.objects.new("Scan", mesh)
bpy.context.scene.objects.link(obj)
mesh.from_pydata(vertices, [], faces)

bpy.data.objects["Cube"].select = True
bpy.ops.object.delete()

bpy.ops.wm.save_mainfile(filepath="scan.blend")

